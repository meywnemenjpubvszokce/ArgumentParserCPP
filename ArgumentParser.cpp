#include "ArgumentParser.h"
#include <string>
#include <iostream>

void ArgumentParser::addArgument(std::string argument) {
    Arguments.insert(std::make_pair(argument, new Argument()));
}

void ArgumentParser::addTag(std::string argument, std::string tag) {
    if (Arguments.find(argument) == Arguments.end()) { // if argument has not been added
        throw Exceptions::ArgumentNotFound();
    }

    Arguments[argument]->pushToTags(tag);
    tagToName.insert(std::make_pair(tag, argument));
}

void ArgumentParser::setMetaVariable(std::string argument, std::string metaVariable) {
    if (Arguments.find(argument) == Arguments.end()) { // if argument has not been added
        throw Exceptions::ArgumentNotFound();
    }

    Arguments[argument]->setMetaVariable(metaVariable);
}

void ArgumentParser::sethelp(std::string argument, std::string help) {
    if (Arguments.find(argument) == Arguments.end()) { // if argument has not been added
        throw Exceptions::ArgumentNotFound();
    }

    Arguments[argument]->setHelp(help);
}

void ArgumentParser::setIsRequired(std::string argument, bool isRequired) {
    if (Arguments.find(argument) == Arguments.end()) { // if argument has not been added
        throw Exceptions::ArgumentNotFound();
    }

    Arguments[argument]->setIsRequired(isRequired);
}

void ArgumentParser::setHasVariable(std::string argument, bool hasVariable) {
    if (Arguments.find(argument) == Arguments.end()) { // if argument has not been added
        throw Exceptions::ArgumentNotFound();
    }

    Arguments[argument]->setHasVariable(hasVariable);
}

std::string ArgumentParser::getDescription() {
    return description;
}

void ArgumentParser::setDescription(std::string description) {
    this->description = description;
}

std::string ArgumentParser::getName() {
    return name;
}

void ArgumentParser::setName(std::string name) {
    this->name = name;
}

void ArgumentParser::help() {
    std::vector<std::string> requiredList;
    std::vector<std::string> optionalList;

    for (auto it = Arguments.begin(); it != Arguments.end(); it++) {
        if (it->second->getIsRequired()) {
            requiredList.push_back(it->first);
        } else {
            optionalList.push_back(it->first);
        }
    }

    unsigned int maxLength = 0;

    for (auto it = requiredList.begin(); it != requiredList.end(); it++) {
        unsigned int length = 0;

        auto tags = Arguments[*it]->getTags();
        for (auto jt = tags->begin(); jt != tags->end(); jt++) {
            length += jt->length() + 2; // add `, `
        }

        length -= 2; // remove last `, `
        length++; // add ` `
        if (Arguments[*it]->getHasVariable()) {
            length += Arguments[*it]->getMetaVariable().length(); // add meta variable length
        }

        if (length > maxLength) {
            maxLength = length;
        }
    }

    for (auto it = optionalList.begin(); it != optionalList.end(); it++) {
        unsigned int length = 0;

        auto tags = Arguments[*it]->getTags();
        for (auto jt = tags->begin(); jt != tags->end(); jt++) {
            length += jt->length() + 2; // add `, `
        }

        length -= 2; // remove last `, `
        length++; // add ` `
        if (Arguments[*it]->getHasVariable()) {
            length += Arguments[*it]->getMetaVariable().length(); // add meta variable length
        }

        if (length > maxLength) {
            maxLength = length;
        }
    }

    if (10 > maxLength) { // 10: length of `-h, --help`
        maxLength = 10;
    }

    std::cout << getName() << std::endl;
    std::cout << getDescription() << std::endl;

    std::cout << "Required" << std::endl;
    for (auto it = requiredList.begin(); it != requiredList.end(); it++) {
        std::string tagsStr;
        
        auto tags = Arguments[*it]->getTags();
        for (auto jt = tags->begin(); jt != tags->end(); jt++) {
            tagsStr += *jt;
            tagsStr += ", ";
        }

        tagsStr.pop_back(); // remove last ` `
        tagsStr.pop_back(); // remove last `,`
        tagsStr += " ";
        if (Arguments[*it]->getHasVariable()) {
            tagsStr += Arguments[*it]->getMetaVariable();
        }

        std::cout << "  ";
        std::cout.width(maxLength);
        std::cout << std::left << tagsStr;
        std::cout << "    " << Arguments[*it]->getHelp() << std::endl;
    }

    std::cout << "Optional" << std::endl;
    for (auto it = optionalList.begin(); it != optionalList.end(); it++) {
        std::string tagsStr;
        
        auto tags = Arguments[*it]->getTags();
        for (auto jt = tags->begin(); jt != tags->end(); jt++) {
            tagsStr += *jt;
            tagsStr += ", ";
        }

        tagsStr.pop_back(); // remove last ` `
        tagsStr.pop_back(); // remove last `,`
        tagsStr += " ";
        if (Arguments[*it]->getHasVariable()) {
            tagsStr += Arguments[*it]->getMetaVariable();
        }

        std::cout << "  ";
        std::cout.width(maxLength);
        std::cout << std::left << tagsStr;
        std::cout << "    " << Arguments[*it]->getHelp() << std::endl;
    }

    std::cout << "  ";
    std::cout.width(maxLength);
    std::cout << std::left << "-h, --help";
    std::cout << "    " << "Display this page" << std::endl;
}

void ArgumentParser::parse(int argc, char **charPointerArgv) {
    std::vector<std::string> argv;

#ifdef NO_ARGUMENT_EXCEPTION
    if (argc == 1) { // if there is no argument
        throw ArgumentParser::Exceptions::NoArgument();
    }
#endif

    for (int i = 0; i < argc; i++) {
        argv.push_back(charPointerArgv[i]);
    }

    for (int i = 1; i < argc; i++) {
        std::string tag = argv[i];

        std::cout << tag << std::endl;
        
        if (tag == "-h" || tag == "--help") {
            help();
            
            exit(0);
        } else if (tagToName.find(tag) == tagToName.end()) {
            throw ArgumentParser::Exceptions::TagNotFound();
        } else {
            Arguments[tagToName[tag]]->setIsEnabled(true);
            
            if (Arguments[tagToName[tag]]->getHasVariable()) {
                if (i + 1 >= argc) {
                    throw ArgumentParser::Exceptions::ArgumentNotFinished();
                } else {
                    Arguments[tagToName[tag]]->setVariable(argv[i + 1]);
                }
                i++;
            }
        }
    }

#ifdef NO_ARG_NO_RBNS
    if (argc > 1) {
#endif
        for (auto it = Arguments.begin(); it != Arguments.end(); it++) {
            if (it->second->getIsRequired() && !it->second->getIsEnabled()) {
                throw Exceptions::RequiredButNotSpecified();
            }
        }
#ifdef NO_ARG_NO_RBNS
    }
#endif
}

std::string ArgumentParser::getVariable(std::string argument) {
    if (Arguments.find(argument) == Arguments.end()) { // if key doesn't exist
        throw Exceptions::ArgumentNotFound();
    } else if (!Arguments[argument]->getIsEnabled()) {
        throw Exceptions::VariableNotAvailable();
    } else {
        return Arguments[argument]->getVariable();
    }
}

bool ArgumentParser::isEnabled(std::string argument) {
    return Arguments[argument]->getIsEnabled();
}
