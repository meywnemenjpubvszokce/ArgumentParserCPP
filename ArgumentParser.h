#pragma once

#include <string>
#include <map>
#include <vector>
#include <exception>

class ArgumentParser {
    private:
        class Argument {
            private:
                std::vector<std::string> tags;
                std::string metaVariable = "FOO";
                std::string help;
                bool isRequired = false;
                bool hasVariable = true;
                bool isEnabled = false;
                std::string variable;

            public:
                std::vector<std::string> *getTags();
                void pushToTags(std::string);
                std::string getMetaVariable();
                void setMetaVariable(std::string);
                std::string getHelp();
                void setHelp(std::string);
                bool getIsRequired();
                void setIsRequired(bool);
                bool getHasVariable();
                void setHasVariable(bool);
                bool getIsEnabled();
                void setIsEnabled(bool);
                std::string getVariable();
                void setVariable(std::string);

        };
        
        std::map<std::string, Argument *> Arguments;
        std::map<std::string, std::string> tagToName;
        std::string description;
        std::string name;

    public:
        void addArgument(std::string argument);
        void addTag(std::string argument, std::string tag);
        void setMetaVariable(std::string argument, std::string mataVariable);
        void sethelp(std::string argument, std::string help);
        void setIsRequired(std::string argument, bool isRequired);
        void setHasVariable(std::string argument, bool hasVariable);
        std::string getDescription();
        void setDescription(std::string description);
        std::string getName();
        void setName(std::string name);
        void help();
        void parse(int argc, char **charPointerArgv);
        
        std::string getVariable(std::string argument);
        bool isEnabled(std::string argument);

        class Exceptions {
            public:
                class TagNotFound {};
                class ArgumentNotFound {};
                class RequiredButNotSpecified {};
                class VariableNotAvailable {};
                class ArgumentNotFinished {};

#ifdef NO_ARGUMENT_EXCEPTION
                class NoArgument {};
#endif
        };
};
