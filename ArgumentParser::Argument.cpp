#include "ArgumentParser.h"

std::vector<std::string> *ArgumentParser::Argument::getTags() {
    return &tags;
}

void ArgumentParser::Argument::pushToTags(std::string tag) {
    tags.push_back(tag);
}

std::string ArgumentParser::Argument::getMetaVariable() {
    return metaVariable;
}

void ArgumentParser::Argument::setMetaVariable(std::string metaVariable) {
    this->metaVariable = metaVariable;
}

std::string ArgumentParser::Argument::getHelp() {
    return help;
}

void ArgumentParser::Argument::setHelp(std::string help) {
    this->help = help;
}

bool ArgumentParser::Argument::getIsRequired() {
    return isRequired;
}

void ArgumentParser::Argument::setIsRequired(bool isRequired) {
    this->isRequired = isRequired;
}

bool ArgumentParser::Argument::getHasVariable() {
    return hasVariable;
}

void ArgumentParser::Argument::setHasVariable(bool hasVariable) {
    this->hasVariable = hasVariable;
}

bool ArgumentParser::Argument::getIsEnabled() {
    return isEnabled;
}

void ArgumentParser::Argument::setIsEnabled(bool isEnabled) {
    this->isEnabled = isEnabled;
}

std::string ArgumentParser::Argument::getVariable() {
    return variable;
}

void ArgumentParser::Argument::setVariable(std::string variable) {
    this->variable = variable;
}
