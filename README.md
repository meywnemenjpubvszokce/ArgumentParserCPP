# ArgumentsParserCPP
Argument parser for C++

+ APIs
    + `void ArgumentParser::addArgument(std::string argument)`
        + add argument
    + `void ArgumentParser::addTag(std::string argument, std::string tag)`
        + add tag to argument
    + `void ArgumentParser::setMetaVariable(std::string argument, std::string metaVariable)`
        + set meta-variable of argument
    + `void ArgumentParser::setHelp(std::string argument, std::string help)`
        + set help message of argument
    + `void ArgumentParser::setIsRequired(std::string argument, bool isRequired)`
        + set whether the argument must be required
    + `void ArgumentParser::setHasVariable(std::string argument, bool hasVariable)`
        + set whether the argument has variable
    + `std::string ArgumentParser::getDescription()`
        + get program's description
    + `void ArgumentParser::setDescription(std::string description)`
        + set program's description
    + `std::string ArgumentParser::getVariable(std::string argument)`
        + get variable of the argument
        + return "" when the argument hasn't been set to hasVariable
    + `std::string ArgumentParser::getName()`
        + get program's name
    + `void ArgumentParser::setName(std::string name)`
        + set program's name
    + `void ArgumentParser::parse(int argc, char **charPointerArgv)`
        + parse arguments with argc, charPointerArgv(commonly argv)
    + `void ArgumentParser::help()`
        + display help page
+ Exceptions
    + `ArgumentParser::Exceptions::TagNotFound`
        + This exception is thrown when not added tag is detected or
        + variable of a tag whose argument has variable was not specified
    + `ArgumentParser::Exceptions::ArgumentNotFound`
        + This exception is thrown when programmer tries to set or get a value of not added argument
    + `ArgumentParser::Exceptions::RequiredButNotSpecified`
        + This exception is thrown when tags are not specified despite the argument is set to required
        + This exception could be disabled when there's no argument by adding `NO_ARG_NO_RBNS` flag when the program is compiled
            + See Flags>NO_ARG_NO_RBNS
    + `ArgumentParser::Exceptions::VariableNotAvailable`
        + This exception is thrown when an argument's tag was not detected or
        + the programmer has been set that the argument does not have variable
    + `ArgumentParser::Exceptions::NoArgument`
        + This exception is thrown when there's no argument
        + This exception could be enabled by adding `NO_ARGUMENT_EXCEPTION` flag when the program is compiled
        + See Flags>NO_ARGUMENT_EXCEPTION
    + `ArgumentParser::Exceptions::ArgumentNotFinished`
        + This exception is thrown when the last tag has variable but argv doesn't have it
+ Flags
    + `NO_ARGUMENT_EXCEPTION`
        + This flag throws `ArgumentParser::Exceptions::NoArgument` when there's no argument
    + `NO_ARG_NO_RBNS`
        + This flag doesn't throw `ArgumentParser::Exceptions::RequiredButNotSpecified` when there's no argument
+ Help page
    + help page can only be displayed by using these tags: `-h` and `--help`
    + if programmer sets those tags manually, they would be ignored
    + Template
        + with variables
            > PROGRAM'S NAME\
              PROGRAM'S DESCRIPTION\
              Required\
              TAGS META-VARIABLES&nbsp;&nbsp;&nbsp;&nbsp;HELP\
              ...\
              TAGS META-VARIABLES&nbsp;&nbsp;&nbsp;&nbsp;HELP\
              Optional\
              TAGS META-VARIABLES&nbsp;&nbsp;&nbsp;&nbsp;HELP\
              ...\
              TAGS META-VARIABLES&nbsp;&nbsp;&nbsp;&nbsp;HELP\
              -h, --help&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Display this page
        + examples
            > ArgumentParser\
              this is wonderful\
              Required\
              -m, --made-with-cpp&nbsp;&nbsp;&nbsp;&nbsp;cpp argument parser\
              -w, --wonderful&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;hahahaha\
              Optional\
              -he, --hello&nbsp;&nbsp;&nbsp;&nbsp;Hello!!!\
              -h, --help&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Display this page
